import React from "react";
import List from "../src/components/List";

export default {
  title: "List"
};

const items = [
  {
    name: "Mario",
    color: "red"
  },
  {
    name: "Luigi",
    color: "green"
  }
];

let itemStyle = {
  display: "flex",
  background: "#d2d2d2",
  margin: 5,
  padding: 5,
  borderRadius: 3
};

let fieldStyle = {
  marginRight: 5,
  background: "white",
  padding: 3,
  borderRadius: 3
};

const itemRenderer = item => (
  <div style={itemStyle}>
    <div style={fieldStyle}>{item.name}</div>
    <div style={fieldStyle}>{item.color}</div>
  </div>
);

export const list = () => <List items={items} itemRenderer={itemRenderer} />;
