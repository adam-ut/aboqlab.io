# Purpose
Okta is used as the SSO authentication mechanism for this application

# References
* [Adobe Documentation](https://wiki.corp.adobe.com/display/dmssecurity/Okta+Integration+How-To)
* [Okta Documentation for React](https://developer.okta.com/quickstart/#/react)
* [Okta Sample Application](https://github.com/okta/samples-js-react)