import * as React from "react";
import {
  H1,
  Spinner,
  Card,
  Text,
  Button,
  Collapse,
  ButtonGroup,
  Intent,
  Tag,
  Divider
} from "@blueprintjs/core";
import { useQuery } from "@apollo/react-hooks";
import { BookRepository, IBook, IUser, BookLink } from "models/Book";
import { Item } from "components/core/List";
import { ThemeContext } from "utils/ThemeContext";

const NameItem: React.FC<{ user: IUser }> = ({ user }) => {
  let itemStyle = {
    height: 30,
    display: "flex",
    alignItems: "center"
  };
  return (
    <div key={user.name} style={itemStyle}>
      <span>{user.name}</span>
      {user.will_lead ? (
        <Tag style={{ marginLeft: 10 }}>Set as lead</Tag>
      ) : null}
    </div>
  );
};

const NameList: React.FC<{ users: IUser[] }> = ({ users }) => {
  let collapsedContentStyle = {
    marginTop: 10
  };
  return (
    <div style={collapsedContentStyle}>
      {users.length ? (
        users.map(user => <NameItem key={user.name} user={user} />)
      ) : (
        <div style={{ display: "flex", justifyContent: "center" }}>
          No sign ups
        </div>
      )}
    </div>
  );
};

interface SubscribedFormat extends BookLink {
  users: string[];
}

const FormatContent: React.FC<{
  subscribedFormats: SubscribedFormat[];
}> = ({ subscribedFormats }) => {
  if (!subscribedFormats.length) {
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        No formats requested
      </div>
    );
  }

  return (
    <div>
      {subscribedFormats.map(({ format, users, link }) => (
        <Tag
          style={{ marginRight: 5 }}
          key={format}
          minimal={true}
          round={true}
        >
          <a
            style={{ marginRight: 5 }}
            href={link || "this.isafakeurl.com/book"}
          >
            {format}
          </a>
          {users.length}
        </Tag>
      ))}
    </div>
  );
};

const CollapsedContent: React.FC<{
  isOpen: boolean;
  subscribedFormats: SubscribedFormat[];
  users: IUser[];
}> = ({ isOpen, subscribedFormats, users }) => (
  <Collapse isOpen={isOpen}>
    <Divider />
    <FormatContent subscribedFormats={subscribedFormats} />
    <Divider />
    <NameList users={users} />
  </Collapse>
);

const SimpleCard: React.FC<{
  book: IBook;
  forceOpen: boolean;
}> = ({ book, forceOpen }) => {
  let [isOpen, setIsOpen] = React.useState(false);
  let collapsedCardStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  };
  let users = book.users;
  let subscribedFormats = book.formats
    .map(({ format, link }) => {
      return {
        format,
        link,
        users: book.users
          .filter(user => user.format === format)
          .map(user => user.name)
      };
    })
    .filter(sf => sf.users.length);

  React.useEffect(() => {
    setIsOpen(forceOpen);
  }, [forceOpen]);

  let lead = book.users.find(user => user.will_lead);

  return (
    <Card
      interactive={true}
      onClick={() => setIsOpen(!isOpen)}
      style={{ maxWidth: 250 }}
      elevation={2}
    >
      <div style={collapsedCardStyle}>
        <Text ellipsize={true}>{book.title}</Text>
        <div>
          <ButtonGroup>
            <Button
              icon="hand"
              minimal={true}
              intent={lead ? Intent.SUCCESS : Intent.NONE}
            ></Button>
            <Button
              icon="person"
              minimal={true}
              intent={users.length >= 3 ? Intent.SUCCESS : Intent.NONE}
            >
              {users ? users.length : 0}
            </Button>
          </ButtonGroup>
        </div>
      </div>
      <CollapsedContent
        isOpen={isOpen}
        subscribedFormats={subscribedFormats}
        users={users}
      />
    </Card>
  );
};

// const itemRenderer = (book: IBook) => {
//   let lead = book.users.find(user => user.will_lead);
//   return <SimpleCard book={book} forceOpen={}/>;
// };

let usersByQuarter: (quarter: string) => (book: IBook) => Item & IBook = (
  quarter: string
) => (book: IBook) => {
  return {
    ...book,
    key: book.title,
    users: book.users ? book.users.filter(user => user.quarter === quarter) : []
  };
};

interface FilterGeneratorParams {
  type: string;
  params?: any[];
}
interface FilterGenerator {
  [k: string]: (...args: any[]) => (i: any) => Item & IBook;
}

let filterGenerators: FilterGenerator = {
  usersByQuarter,
  all: () => (i: IBook): Item & IBook => ({
    ...i,
    key: i.title,
    users: i.users ? i.users : []
  })
};

const CardList: React.FC<{ books: IBook[]; forceOpen: boolean }> = ({
  books,
  forceOpen
}) => {
  const listItemStyle = {
    margin: "10px 0px"
  };
  return (
    <div>
      {books.map(book => (
        <div key={book.title} style={listItemStyle}>
          <SimpleCard book={book} forceOpen={forceOpen} />
        </div>
      ))}
    </div>
  );
};

const FilterableList: React.FC<{ books: IBook[] }> = ({ books }) => {
  let quarters = ["4Q2019", "1Q2020"];

  let [quarter, setQuarter] = React.useState<string>(quarters[0]);
  let [forceOpen, setForceOpen] = React.useState(false);

  let [filterParams, setFilterParams] = React.useState<FilterGeneratorParams>({
    type: "usersByQuarter",
    params: [quarter]
  });
  let filterGenerator = filterGenerators[filterParams.type];
  let filter = filterGenerator(
    ...(filterParams.params ? filterParams.params : [])
  );
  let booksToRender = books.map(filter);

  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <ButtonGroup>
          {quarters.map(_quarter => (
            <Button
              key={_quarter}
              onClick={() => {
                setQuarter(_quarter);
                setFilterParams({ type: "usersByQuarter", params: [_quarter] });
              }}
              active={quarter === _quarter}
            >
              {_quarter}
            </Button>
          ))}
        </ButtonGroup>
        <ButtonGroup>
          <Button
            onClick={() => {
              setForceOpen(!forceOpen);
            }}
          >
            {forceOpen ? "Collapse All" : "Expand All"}
          </Button>
        </ButtonGroup>
      </div>
      <CardList books={booksToRender} forceOpen={forceOpen} />
    </>
  );
};

const AdminView = () => {
  const { loading, data } = useQuery(BookRepository.BOOKS_QUERY, {
    notifyOnNetworkStatusChange: true
  });

  const useDarkTheme = React.useContext(ThemeContext);

  return (
    <div>
      <H1>Admin</H1>
      <div className={"box" + (useDarkTheme ? " dark" : "")}>
        {loading ? <Spinner /> : <FilterableList books={data.books} />}
      </div>
    </div>
  );
};

export default AdminView;
