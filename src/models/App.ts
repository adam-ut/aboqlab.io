import initializeApolloClient from "utils/initializeApolloClient";

export const client = initializeApolloClient();
